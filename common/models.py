from django.db import models


class RecordedModel(models.Model):
    created = models.DateTimeField('Inner created at', auto_now_add=True)
    updated = models.DateTimeField('Inner updated at', auto_now=True)

    class Meta:
        abstract = True
