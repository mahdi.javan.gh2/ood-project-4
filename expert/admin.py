from django.contrib import admin
from expert.models import Expert, Expertise, Degree

admin.site.register(Expert)
admin.site.register(Expertise)
admin.site.register(Degree)
