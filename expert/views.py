from django.shortcuts import render
from .models import Expertise, Skill, Expert, Degree


def skills(request):
    if request.method == 'POST':
        exp = request.POST.get('exp')
        if exp and not Skill.objects.filter(expert__user=request.user, expertise_id=exp).exists():
            expert = Expert.objects.get(user=request.user)
            expertise = Expertise.objects.get(id=exp)
            skill = Skill.objects.create(expertise=expertise, expert=expert)
            Degree.objects.create(skill=skill, file=request.FILES.get('degree'))
    exps = Expertise.objects.all()
    current_exps = Skill.objects.select_related('expertise', 'degree').filter(expert__user_id=request.user.id)
    return render(
        request,
        'skills.html',
        {
            'exps': exps,
            'current_skills': current_exps,
            'title': 'Skills'
        }
    )
