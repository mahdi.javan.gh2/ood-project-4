from django.db import models
from account.models import User
from common.models import RecordedModel


class Expert(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='expert'
    )

    balance = models.FloatField(
        default=0
    )


class Expertise(RecordedModel):
    name = models.TextField(
        null=True,
        blank=False
    )

    description = models.TextField(
        null=True,
        blank=True
    )

    price = models.FloatField(
        default=0
    )

    def __str__(self):
        return f'{self.name} - {self.price}'


class Skill(RecordedModel):
    expert = models.ForeignKey(
        Expert,
        on_delete=models.CASCADE,
        related_name='skills'
    )

    expertise = models.ForeignKey(
        Expertise,
        on_delete=models.CASCADE,
        related_name='skills'
    )


class Degree(models.Model):
    skill = models.OneToOneField(
        Skill,
        on_delete=models.CASCADE,
    )

    file = models.FileField(
        upload_to='degrees',
        null=True,
        blank=True,
    )

    is_qualified = models.BooleanField(
        default=False
    )

    def __str__(self):
        user = self.skill.expert.user
        return f'{user.first_name} {user.last_name} - {self.skill.expertise.name}'
