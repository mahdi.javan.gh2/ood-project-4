from django.shortcuts import redirect
from .models import Feedback


def create(request):
    order_id = request.POST.get('order_id')
    Feedback.objects.create(
        user=request.user,
        order_id=order_id,
        content=request.POST.get('content'),
        type=request.POST.get('type'),
        rate=request.POST.get('rate')
    )
    return redirect('single_order', order_id)
