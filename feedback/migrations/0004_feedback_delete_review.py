# Generated by Django 4.0.5 on 2022-08-19 15:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_order_created_order_updated'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('feedback', '0003_review_created_review_updated'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Inner created at')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Inner updated at')),
                ('rate', models.IntegerField(default=5)),
                ('content', models.TextField(blank=True, null=True)),
                ('type', models.CharField(choices=[('review', 'review'), ('msg', 'msg')], max_length=7)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rates', to='order.order')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rates', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.DeleteModel(
            name='Review',
        ),
    ]
