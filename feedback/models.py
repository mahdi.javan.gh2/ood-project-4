from django.db import models
from order.models import Order
from account.models import User
from common.models import RecordedModel


class Feedback(RecordedModel):
    REVIEW_TYPE = 'review'
    MSG_TYPE = 'msg'
    FEEDBACK_TYPES = (
        (REVIEW_TYPE, REVIEW_TYPE),
        (MSG_TYPE, MSG_TYPE),
    )
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='rates'
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='rates'
    )

    rate = models.IntegerField(
        default=5
    )

    content = models.TextField(
        blank=True,
        null=True,
    )

    type = models.CharField(
        choices=FEEDBACK_TYPES,
        max_length=7
    )
