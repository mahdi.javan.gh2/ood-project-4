from django.shortcuts import render, redirect
from .models import Order
from expert.models import Expertise, Skill
from feedback.models import Feedback


def done_orders(request):
    filters = {}
    if request.user.is_expert:
        filters['expert__user'] = request.user
    else:
        filters['customer__user'] = request.user
    orders = Order.objects.filter(status=Order.STATUS_DONE, **filters)
    return render(request, 'done_orders.html', {'orders': orders})


def orders_list(request):
    if request.method == 'POST':
        exp = Expertise.objects.get(id=request.POST.get('exp'))
        description = request.POST.get('description')
        Order.objects.create(expertise=exp, customer=request.user.customer, description=description)
    exps = Expertise.objects.all()
    orders = Order.objects.filter(status__in=Order.get_active_status())
    pending_orders = []
    if request.user.is_customer:
        orders = orders.filter(customer=request.user.customer)
        template_file = 'orders.html'
    else:
        template_file = 'expert_orders.html'
        orders = orders.filter(expert=request.user.expert)
        exp_ids = list(
            Skill.objects.filter(
                expert=request.user.expert, degree__is_qualified=True
            ).values_list('expertise_id', flat=True)
        )
        pending_orders = Order.objects.filter(
            expert__isnull=True, status=Order.STATUS_PENDING, expertise_id__in=exp_ids
        )
    context = {'orders': orders, 'title': 'Orders', 'exps': exps, 'pending_orders': pending_orders}
    return render(request, template_file, context)


def single_order(request, oid):
    order = Order.objects.get(id=oid)
    if request.method == 'POST':
        if request.user.is_expert:
            expert = request.user.expert
            if order.status == 1:
                order.status = 2
                order.save(update_fields=['status'])
                order.expert.balance += order.expertise.price
                order.expert.save()
            else:
                order.expert = expert
                order.save(update_fields=['expert'])
                return redirect('orders_list')
        else:
            rejected = request.GET.get('q') == 'true'
            if rejected:
                order.expert = None
                order.save()
            else:
                order.status = Order.STATUS_DOING
                order.save(update_fields=['status'])
                return redirect('orders_list')
    feedbacks = Feedback.objects.filter(
        order_id=order.id,
        type=Feedback.MSG_TYPE,
    )
    review = Feedback.objects.filter(user=request.user, order_id=oid, type=Feedback.REVIEW_TYPE).first()
    return render(
        request,
        'single_order.html',
        {
            'order': order,
            'feedbacks': feedbacks,
            'review': review
        }
    )
