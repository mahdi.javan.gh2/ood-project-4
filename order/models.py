from django.db import models
from expert.models import Expert, Expertise
from customer.models import Customer
from common.models import RecordedModel


class Order(RecordedModel):
    STATUS_PENDING = 0
    STATUS_DOING = 1
    STATUS_DONE = 2
    STATUS_CANCELLED = 3

    STATUS_CHOICES = (
        (STATUS_PENDING, 'PENDING'),
        (STATUS_DOING, 'DOING'),
        (STATUS_DONE, 'DONE'),
        (STATUS_CANCELLED, 'CANCELLED'),
    )

    expert = models.ForeignKey(
        Expert,
        on_delete=models.DO_NOTHING,
        related_name='orders',
        null=True,
    )

    expertise = models.ForeignKey(
        Expertise,
        on_delete=models.DO_NOTHING,
        related_name='expertises',
        null=True,
    )

    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='orders'
    )

    status = models.IntegerField(
        default=0,
        choices=STATUS_CHOICES
    )

    description = models.TextField(
        default='',
        blank=True,
    )

    @classmethod
    def get_active_status(cls):
        return [cls.STATUS_DOING, cls.STATUS_PENDING]
