from django.urls import path
from . import views

urlpatterns = [
    path('list/', views.orders_list, name='orders_list'),
    path('<int:oid>/', views.single_order, name='single_order'),
    path('done_orders/', views.done_orders, name='done_orders'),
]
