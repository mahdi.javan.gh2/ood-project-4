from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm


def signin(request):
    form = None
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        form.full_clean()
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(password)
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if user.is_customer:
                return redirect('profile')
            elif user.is_expert:
                return redirect('skills')
    errors = []
    if form:
        for e in form.errors.values():
            errors.extend(e)
    context = {'title': 'Log in', 'errors': errors}
    return render(request, 'login.html', context)
