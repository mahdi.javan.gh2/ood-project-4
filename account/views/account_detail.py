from django.db.models import Q
from django.shortcuts import render

from account.models import User
from order.models import Order
from feedback.models import Feedback


def account_detail(request, uid):
    orders = Order.objects.filter(Q(expert__user_id=uid) | Q(customer__user_id=uid), status=Order.STATUS_DONE)
    reviews = Feedback.objects.filter(
        type=Feedback.REVIEW_TYPE, order__in=orders
    ).exclude(user_id=uid)
    rates = list(reviews.values_list('rate', flat=True))
    context = {
        'account': User.objects.get(id=uid),
        'rate': f'{sum(rates) / len(rates)}/5' if reviews else 'امتیازی برای کاربر ثبت نشده است',
        'orders_count': orders.count(),
        'reviews': reviews
    }
    return render(request, 'account_detail.html', context)
