from django.shortcuts import render, redirect

from customer.models import Customer
from expert.models import Expert
from ..forms import AccountCreationForm


def sign_up(request):
    if request.method == "POST":
        form = AccountCreationForm(request.POST)
        if form.is_valid():
            is_customer = request.POST.get('type')
            user = form.save()
            if is_customer == '1':
                Customer.objects.create(user=user)
            else:
                Expert.objects.create(user=user)
            return redirect('signin')
    else:
        form = AccountCreationForm()
    errors = []
    for e in form.errors.values():
        errors.extend(e)
    print(form.data)
    context = {'title': 'Sign up', 'form': form, 'errors': errors}
    return render(request, 'register.html', context)
