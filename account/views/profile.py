from django.shortcuts import render

from ..forms import AccountChangeForm


def profile(request):
    if request.method == "POST":
        form = AccountChangeForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
    else:
        form = AccountChangeForm(instance=request.user)
    context = {'title': 'Profile', 'form': form}
    print(context)
    return render(request, 'profile.html', context)
