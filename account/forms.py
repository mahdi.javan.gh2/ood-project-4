from django.contrib.auth.forms import UserCreationForm
from django import forms

from .models import User


class AccountCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = User.get_form_fields()


class AccountChangeForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['phone_number', 'first_name', 'last_name', 'email', 'avatar']
