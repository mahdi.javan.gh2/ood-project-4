from django.urls import path
from . import views


urlpatterns = [
    path('signup/', views.sign_up, name='signup'),
    path('signin/', views.signin, name='signin'),
    path('profile/', views.profile, name='profile'),
    path('logout/', views.log_out, name='logout'),
    path('<int:uid>/', views.account_detail, name='account_detail'),
]
