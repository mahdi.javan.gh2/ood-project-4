from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    GENDER_MALE = 'male'
    GENDER_FEMALE = 'female'
    GENDER_TYPES = (
        (GENDER_MALE, GENDER_MALE),
        (GENDER_FEMALE, GENDER_FEMALE),
    )

    national_id = models.CharField(
        max_length=11,
        unique=True,
        verbose_name='کد ملی'
    )

    gender = models.CharField(
        max_length=6,
        choices=GENDER_TYPES,
        verbose_name='جنسیت'
    )

    phone_number = models.CharField(
        max_length=11,
        unique=True,
        verbose_name='شماره تلفن'
    )

    avatar = models.ImageField(
        null=True,
        blank=True,
        upload_to='profile_pics',
        default='profile_pics/default.jpeg'
    )

    @staticmethod
    def get_form_fields():
        return ['username', 'email', 'first_name', 'last_name', 'phone_number', 'national_id', 'gender']

    @property
    def is_expert(self):
        from expert.models import Expert
        return Expert.objects.filter(user=self).exists()

    @property
    def is_customer(self):
        from customer.models import Customer
        return Customer.objects.filter(user=self).exists()
